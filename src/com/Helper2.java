package com;

/**
 * Created by joy on 6/13/17.
 */
public class Helper2 {

    private int num1;
    private int num2;
    private int choice;
    private double ans;

    public int getNum1() {
        return num1;
    }
    

    public void setNum1(int num1) {
        this.num1 = num1;
    }

    public int getNum2() {
        return num2;
    }

    public void setNum2(int num2) {
        this.num2 = num2;
    }

    public double getAns() {
        return ans;
    }

    public void setAns(float ans) {
        this.ans = ans;
    }

    public Helper2(){

        num1 = 0;
        num2 = 0;
        choice = 0;
        //ans = 0;
    }
    public int getChoice(){
        return choice;
    }

    public void setChoice(int choice){
        this.choice = choice;
    }

    public void calculation(int choice){
        switch (choice) {
            case 1:
                ans = num1 + num2;
                System.out.println("Output :" + ans);
                break;

            case 2:
                ans = num1 - num2;
                System.out.println("Output :" + ans);
                break;

            case 3:
                ans = num1 * num2;
                System.out.println("Output :" + ans);
                break;

            case 4:
                try {
                    ans = (float)num1 / num2;
                    System.out.println("Output :" + ans);

                } catch (ArithmeticException e) {
                    e.printStackTrace();
                }
                break;

            default:
                System.out.println("Please Enter Valid key");
                break;
        }
    }
}
